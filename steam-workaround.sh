#!/usr/bin/sh

# default steam path
steampath="$HOME/.steam/steam/"

# hacky positional args
[ -n "$1" ] && {
	steampath=$1
}

[ -z "$2" ] && {
	linux_manifest="6793017837429788461"
	content_manifest="2232945182712745816"
}
[ "$2" = "--v1.26.7" ] && {
	linux_manifest="8898091582812370409"
	content_manifest="6969399504958588762"
}

# check for steamcmd
which steamcmd || {
	echo "install SteamCMD to use this script"
	exit 1
}

# check that the steampath is installable to
[ ! -d "$steampath/ubuntu12_32" ] && {
	echo "invalid steam path. try another dir"
	exit 1
}

# check that the stuff is already installed
[ -d $steampath/ubuntu12_32/steamapps/content/app_250820/depot_250823 ] && vr_installed=1
[ -d $steampath/ubuntu12_32/steamapps/content/app_250820/depot_250824 ] && content_installed=1

# allow forcing a reinstall
[ -n "$FORCE_REINSTALL" ] && {
	rm -rf $steampath/ubuntu12_32/steamapps/content/app_250820/depot_250823
	rm -rf $steampath/ubuntu12_32/steamapps/content/app_250820/depot_250824
	unset vr_installed
	unset content_installed
}

[ ! "$vr_installed" ] && [ ! "$content_installed" ] && {
	steamcmd +runscript <<< "download_depot 250820 250823 $linux_manifest
	download_depot 250820 250824 $content_manifest"
	pid=$!
	wait $pid
	echo "VR version downloaded."
}

# apply to installation
echo "Applying to current installation..."

# thanks vermeeren
# https://github.com/ValveSoftware/SteamVR-for-Linux/issues/623#issuecomment-1780358506

cd $steampath/steamapps/common/SteamVR/bin
rm -rf linux64
cp -a $steampath/ubuntu12_32/steamapps/content/app_250820/depot_250823/bin/linux64 .
rm -rf vrwebhelper
cp -a $steampath/ubuntu12_32/steamapps/content/app_250820/depot_250823/bin/vrwebhelper .

# don't delete here, add and overwrite only
cd $steampath/steamapps/common/SteamVR
cp -a $steampath/ubuntu12_32/steamapps/content/app_250820/depot_250824/resources .
