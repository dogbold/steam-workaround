# Steam Workaround Script
This script automates a workaround to a current SteamVR bug, found here: https://github.com/ValveSoftware/SteamVR-for-Linux/issues/623#issuecomment-1780358506
The workaround is to download SteamVR version 1.27.5 and then install it manually. Why do that manually when you could throw this in your launch options?

# Dependencies
- SteamCMD
- Bash
